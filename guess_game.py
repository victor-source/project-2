import random
print("This is a guess number game!! \n")
#declare hidden variable
hidden=random.randrange(1,10)
print(hidden)
#ask player to guess 3 times
for guess_count in range (1,4):
	guesses= int(input("\n enter your first guess"))
	if guesses != hidden:
		print("\n wrong guess try again \n")
	elif guesses == hidden:
		print(f"you won in {guess_count} guesses")
		break